import Home from '../../screen/home/Home'
import DetailHome from '../../screen/home/DetailHome'
import {createStackNavigator} from 'react-navigation-stack'
const home = createStackNavigator({
   home: {
      screen: Home,
      navigationOptions: {
         headerShown: false,

      }
   },
   detailHome: {
      screen: DetailHome,
      navigationOptions: {
         headerShown: false,

      }
   },
})

export default home