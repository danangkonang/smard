import React,{Component} from 'react'
import {
   ActivityIndicator,
   StatusBar,
   View,
} from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
export default class App extends Component {
   componentDidMount() {
      this._bootstrapAsync()
   }

   _bootstrapAsync = async () => {
      const userToken = await AsyncStorage.getItem('userToken')
      setTimeout(()=>{
         this.props.navigation.navigate(userToken ? 'main' : 'login')
      },5000)
   }

   render() {
      return (
         <View style={{flex:1,backgroundColor:'tomato',alignItems:'center',justifyContent:'center'}}>
            <ActivityIndicator />
            <StatusBar barStyle="default" />
         </View>
      )
    }
}