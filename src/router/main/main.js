import React from 'react'
import { Text } from 'react-native'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import MCI from 'react-native-vector-icons/MaterialCommunityIcons'
import HomeRoute from './homeRoute'
import AccuntRoute from './accuntRoute'
import FundRoute from './fundRoute'
import CartRoute from './cartRoute'
import NewsRoute from './newsRoute'
const Main = createBottomTabNavigator(
   {
      homeRoute: {
         screen: HomeRoute,
         navigationOptions:({navigation})=>{
            if (navigation.state.index > 0){
               return({tabBarVisible:false})
            }
            return({
               tabBarLabel: ({ focused }) => (
                  <Text style={{ textAlign: 'center', fontSize: 12, color: focused ? "#314fa1" : "#7f7f7f", marginBottom: 4, }}>
                     Home
                  </Text>
               ),
               tabBarIcon: ({ focused }) => (
                  focused ?<MCI name="home" size={25} color="tomato"/>:<MCI name="home" size={25} color="tomato"/>
               ),
            })
         }
      },
      accuntRoute: {
         screen: AccuntRoute,
         navigationOptions:({navigation})=>{
            if (navigation.state.index > 0){
               return({tabBarVisible:false})
            }
            return({
               tabBarLabel: ({ focused }) => (
                  <Text style={{ textAlign: 'center', fontSize: 12, color: focused ? "#314fa1" : "#7f7f7f", marginBottom: 4, }}>
                     Stok
                  </Text>
               ),
               tabBarIcon: ({ focused }) => (
                  focused ?<MCI name="home" size={25} color="tomato"/>:<MCI name="home" size={25} color="tomato"/>
               ),
            })
         }
      },
      fundRoute: {
         screen: FundRoute,
         navigationOptions:({navigation})=>{
            if (navigation.state.index > 0){
               return({tabBarVisible:false})
            }
            return({
               tabBarLabel: ({ focused }) => (
                  <Text style={{ textAlign: 'center', fontSize: 12, color: focused ? "#314fa1" : "#7f7f7f", marginBottom: 4, }}>
                     Fund
                  </Text>
               ),
               tabBarIcon: ({ focused }) => (
                  focused ?<MCI name="home" size={25} color="tomato"/>:<MCI name="home" size={25} color="tomato"/>
               ),
            })
         }
      },
      cartRoute: {
         screen: CartRoute,
         navigationOptions:({navigation})=>{
            if (navigation.state.index > 0){
               return({tabBarVisible:false})
            }
            return({
               tabBarLabel: ({ focused }) => (
                  <Text style={{ textAlign: 'center', fontSize: 12, color: focused ? "#314fa1" : "#7f7f7f", marginBottom: 4, }}>
                     Cart
                  </Text>
               ),
               tabBarIcon: ({ focused }) => (
                  focused ?<MCI name="home" size={25} color="tomato"/>:<MCI name="home" size={25} color="tomato"/>
               ),
            })
         }
      },
      newsRoute: {
         screen: NewsRoute,
         navigationOptions:({navigation})=>{
            if (navigation.state.index > 0){
               return({tabBarVisible:false})
            }
            return({
               tabBarLabel: ({ focused }) => (
                  <Text style={{ textAlign: 'center', fontSize: 12, color: focused ? "#314fa1" : "#7f7f7f", marginBottom: 4, }}>
                     News
                  </Text>
               ),
               tabBarIcon: ({ focused }) => (
                  focused ?<MCI name="home" size={25} color="tomato"/>:<MCI name="home" size={25} color="tomato"/>
               ),
            })
         }
      }
   }
)
export default Main