import Fund from '../../screen/fund/Fund'
import {createStackNavigator} from 'react-navigation-stack'
const fund = createStackNavigator({
   fund: {
      screen: Fund,
      navigationOptions: {
         headerShown: false,

      }
   }
})

export default fund