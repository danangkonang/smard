import Cart from '../../screen/cart/Cart'
import {createStackNavigator} from 'react-navigation-stack'
const cart = createStackNavigator({
   cart: {
      screen: Cart,
      navigationOptions: {
         headerShown: false,

      }
   }
})

export default cart