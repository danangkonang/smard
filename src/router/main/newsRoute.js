import News from '../../screen/news/News'
import {createStackNavigator} from 'react-navigation-stack'
const news = createStackNavigator({
   news: {
      screen: News,
      navigationOptions: {
         headerShown: false,

      }
   }
})

export default news