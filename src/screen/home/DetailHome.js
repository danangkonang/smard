import React from 'react'
import { 
   View,
   Text,
   TouchableOpacity
} from 'react-native'
const App =(props)=>{
   return(
      <View style={{flex:1,justifyContent:'center'}}>
         <Text style={{textAlign:'center'}}>Detail home</Text>
         <TouchableOpacity 
            onPress={()=>props.navigation.navigate('home')}
            style={{backgroundColor:'tomato',marginHorizontal:20,paddingVertical:10,alignItems:'center'}}>
            <Text>Back</Text>
         </TouchableOpacity>
      </View>
   )
}
export default App