import React,{useState,useEffect} from 'react'
import { 
   View,
   Text,
} from 'react-native'
import BottomMenu from '../component/BottomMenu'
import Home from '../screen/home/Home'
import Fund from '../screen/fund/Fund'
const App =(props)=>{
   const[state,setState]=useState({
      active:true
   })
   console.log(props)
   return(
      <View style={{flex:1,backgroundColor:'tomato'}}>
         <View style={{flex:1}}>
            <Home/>
         </View>
         <BottomMenu navigation={props}/>
      </View>
   )
}
export default App