import React,{useState} from 'react'
import { 
   View,
   Text,
   TouchableOpacity,
   ImagePropTypes
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
const App =(props)=>{
   const [state,setState]=useState({
      menu:[
         {
            iconAtive:"home",
            iconNonActive:"home-outline",
            title:"Home",
            navigate:"home"
         },
         {
            iconAtive:"signal-cellular-3",
            iconNonActive:"signal-cellular-outline",
            title:"Fund",
            navigate:"fund"
         },
         {
            iconAtive:"cart",
            iconNonActive:"cart-outline",
            title:"Cart",
            navigate:"cart"
         },
         {
            iconAtive:"script-text",
            iconNonActive:"script-text-outline",
            title:"News",
            navigate:"news"
         },
         {
            iconAtive:"account-circle",
            iconNonActive:"account-circle-outline",
            title:"Accunt",
            navigate:"accunt"
         }
      ]
   })
   return(
      <View style={{flexDirection:'row',backgroundColor:'#FFFFFF'}}>
         {
            state.menu.map((item,i)=>(
               <View style={{flex:1,alignItems:'center',justifyContent:'center',paddingVertical:5}} key={i}>
                  <TouchableOpacity
                  onPress={()=>{
                     props.navigation.navigation.navigate(item.navigate)
                  }}
                  style={{alignItems:'center'}}>
                     <Icon 
                        name={props.navigation.route.name===item.navigate?item.iconAtive:item.iconNonActive} 
                        size={23} 
                        color={'#1C1A46'} 
                        style={{ marginTop: 5 }} />
                     <Text style={{fontSize:12}}>{item.title}</Text>
                  </TouchableOpacity>
               </View>
            ))
         }
      </View>
   )
}
export default App