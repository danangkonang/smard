import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import Auth from './auth/auth';
import Main from './main/main';

export default createAppContainer(
   createSwitchNavigator(
      { 
         main: Main,
         auth: Auth
      },
      {
         initialRouteName: 'auth',
      }
   )
);