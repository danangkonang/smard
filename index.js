/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './src/router';
// import App from './src/router/main/main'
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
