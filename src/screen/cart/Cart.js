import React from 'react'
import { 
   View,
   Text,
} from 'react-native'
import BottomMenu from '../../component/BottomMenu'
const App =(props)=>{
   return(
      <View style={{flex:1,backgroundColor:'tomato'}}>
         <View style={{flex:1}}>
            <Text>Class app cart</Text>
         </View>
         <BottomMenu navigation={props}/>
      </View>
   )
}
export default App