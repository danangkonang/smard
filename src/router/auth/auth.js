import {createStackNavigator} from 'react-navigation-stack'
import Login from '../../screen/auth/Login'
import Registrasi from '../../screen/auth/Registrasi'
import AuthLoading from '../../screen/auth/AuthLoading'
const stockStack = createStackNavigator(
   {
      login: {
         screen: Login,
         navigationOptions: {
            headerShown: false,

         }
      },
      authLoading: {
         screen: AuthLoading,
         navigationOptions: {
               headerShown: false
         }
      },
      registrasi: {
         screen: Registrasi,
         navigationOptions: {
            headerShown: false
         }
      },
   },
   {
      initialRouteName: 'authLoading',
   }
)

export default stockStack
