import Accunt from '../../screen/accunt/Accunt'
import DetailAccunt from '../../screen/accunt/DetailAccunt'
import {createStackNavigator} from 'react-navigation-stack'
const accunt = createStackNavigator({
   accunt: {
      screen: Accunt,
      navigationOptions: {
         headerShown: false,

      }
   },
   detailAccunt: {
      screen: DetailAccunt,
      navigationOptions: {
         headerShown: false,

      }
   },
})

export default accunt